import React  from 'react';
import styled from 'styled-components'
import './App.css';


import Routes from './routes'

const Wrapper = styled.div`
    width: 90%; 
    max-width: 1000px; 
    margin: 0 auto; 
`;


function App() {
  return (
    <Wrapper>
      <Routes />
    </Wrapper>  
    
  );
}

export default App;
