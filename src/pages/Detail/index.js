import React, { Component } from 'react';
import ReactPlayer from "react-player";
import styled from 'styled-components'
import ErrorContent from '../../components/ErrorContent'
import Loading from '../../components/Loading'
import Moment from 'react-moment';
import {url} from '../../utils/config'
import {device} from '../../utils/devices'
const DetailContent = styled.div`
    display: flex; 
    flex-direction: column;
`;
const ImageContent = styled.div`
        text-align: center;
        div {
            max-width: 100%; 
            margin: 0 auto;
            
        }    
 
`

const Image = styled.img `
    width: 100%;
    height: auto;        
` 
const TextContent = styled.div `
    border-left: 5px solid #fff;
    padding: 0 0 0 20px;
    @media ${device.md} {
        margin: 0 0 0 5%;
    }
`;
const Title = styled.h3 `
    font-size: 1.2rem;
    color: #fff;
    font-weight: normal; 
`

const Text = styled.p `
    color: #fff; 
`

const Date = styled.div `
    color: #fff; 
    letter-spacing: 1px;
`
class Detail extends Component {
    constructor(props) {
        super(props); 
        this.state = {
            error: null,
            isLoaded: false,
            item: []
        }
    }


    componentDidMount() {
        this.loadItem();
        /*const { date } = this.props.match.params;
        fetch(`${url}&start_dae=2020-08-22&end_date=2020-09-01`, {
            headers: {'Content-Type': 'application/json' }
        })
        .then((res) => res.json())
        .then(data => {
            this.setState({
                item: data,
                isLoaded: true,
            });
            
        })
        .catch(error => this.setState({ error, isLoading: false }));   */
    }

    loadItem() {
        const { date } = this.props.match.params;
        fetch(`${url}&date=${date}`, {
            headers: {'Content-Type': 'application/json',
            credentials: '*', },
        })
        .then(res => res.json())
        .then((result) => {
            if(result.url) {
                    this.setState({
                    isLoaded: true,
                    item: result
                });
            }    
            else {
                this.setState({ error: true, isLoading: false })
            }
  
        })
        .catch(error => this.setState({ error, isLoading: false }));
    }
    render() {
        const { error, isLoaded, item } = this.state;
        if (error) {
            return (
                <ErrorContent/>
            );
        }
        else if (!isLoaded) {
            return (
                <Loading/>
            )
        }
        
        else {
            return(
                <DetailContent>
                    <ImageContent>
                        {(item.url).includes('youtube') 
                            ? <ReactPlayer url={item.url} />
                            : <Image src={item.url} alt={item.title} />
                        }
                    </ImageContent>
                    <TextContent>
                        <Title>{item.title}</Title>
                        <Date><Moment format="DD.MM.YYYY">{item.date}</Moment></Date>
                        <Text>{item.explanation}</Text>
                    </TextContent>    

                </DetailContent>
            )
        }    
    }
}

export default Detail;