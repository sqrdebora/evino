import React, { Component } from 'react'; 
import Moment from 'react-moment';
import moment from 'moment'
import ReactPlayer from "react-player";
import { Link } from 'react-router-dom';
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowRight } from '@fortawesome/free-solid-svg-icons'

import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';

import ErrorContent from '../../components/ErrorContent'
import Loading from '../../components/Loading'

import {url} from '../../utils/config'
import {device} from '../../utils/devices'
import {colors} from '../../utils/colors'

const Gallery = styled.div`
  column-count: 1;
  column-gap: .9em;
  @media ${device.md} {
    column-count: 3;
  }
  a {
      text-decoration: none;
  }
`;

const GalleryItem = styled.div`
  overflow: hidden; 
  margin: 0 0 50px 0;
  break-inside: avoid-column;
  &:hover h3::after {
      height: 50px; 
  }
`;

const ItemDate = styled.div`
    color: #fff; 

`;

const ItemTitle = styled.h3`
  font-size: 1.2rem; 
  letter-spacing: 1px; 
  margin: 0; 
  color: #fff;
  font-weight: normal;
  position: relative; 
  padding: 0 0 0 20px;
  &::before, 
  &::after
  {
    position: absolute; 
    top: -20px; 
    left: 8px;
    z-index: 24; 
    content: '';
    display: block;  
    background: #fff; 
    width: 5px; 
    height: 50px; 
  } 
  &::after {
      height: 0; 
      background: #37ABF9;
      transition: 0.5s all ease; 
  } 
`;


const ItemImageContent = styled.div `
  position: relative;
`;

const ItemImage = styled.div`
  min-height: 150px;
   div {
    max-width: 100%
  }
  &::after {
    content: ''; 
    display: block;
    width: 100%; 
    height: 150px;
    background: #ddd;
    position: absolute;
    top: 0;
    z-index: -1;
  }  
  img{ 
    width: 100%; 
    height: auto; 
  }
`;

const ItemLink = styled.div`
    width: 50px; 
    height: 50px;
    border-radius: 50%; 
    text-align: center;
    background: ${colors.default};
    position: absolute; 
    bottom: 15px;
    right: 5px;  
    margin: 0; 
    width: 35px;
    height: 35px;
    color: #fff; 
`

const LinkIcon = styled.div`
  color: #fff;
  position: absolute;
  top: 50%; 
  left: 50%; 
  transform: translate(-50%,-50%);
`;



class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: [],
            startDate: null,
            endDate: null, 
            prevY: 0
            //dateFormatted : Moment().locale('fr').format('dddd Do MMMM YYYY HH:mm:ss').toString()
        };

        this.loadItems = this.loadItems.bind(this)
    }

    componentDidMount() {
        this.loadItems();
        
        
        
    }
    loadItems() {
        
        var today = moment(); 
        var endDate = today.format('YYYY-MM-DD')
        var startDate = today.subtract(30, 'days').format('YYYY-MM-DD')
        
        fetch(`${url}&start_date=${startDate}&end_date=${endDate}`, {
            headers: {'Content-Type': 'application/json',
            credentials: '*', },
        })
        .then(res => res.json())
        .then((result) => {
            this.setState({
            isLoaded: true,
            items: result.reverse()
            });
        })
        .catch(error => this.setState({ error, isLoading: false }));
    }

    
    render() {
        const { error, isLoaded, items } = this.state;
        if (error) {
            return (
                <ErrorContent/>
            );
        }
        else if (!isLoaded) {
            return (
                <Loading/>
            )
        }
        
        else {
            return( 
                <Gallery >
                    {items.map((item => {
                        return(
                                <GalleryItem key={item.date}>
                                    <Link to={`/detail/${item.date}`}>
                                        <ItemImageContent>
                                            <ItemImage>
                                                {item.url.includes('youtube') 
                                                    ? <ReactPlayer url={item.url} />
                                                    : <LazyLoadImage effect="blur" src={item.url} alt={item.title} />
                                                }
                                            </ItemImage>
                                            <ItemLink>
                                                <LinkIcon><FontAwesomeIcon icon={faArrowRight} /></LinkIcon>
                                            </ItemLink> 
                                        </ItemImageContent>
                                        <ItemTitle>
                                            <ItemDate><Moment format="DD.MM.YYYY">{item.date}</Moment></ItemDate>
                                            {item.title}
                                        </ItemTitle>  
                                    </Link>
                                </GalleryItem>   
                                
                        )
                    }))}
                    
                    <div ref={loadingRef => (this.loadingRef = loadingRef)}>
                        <span>Loading...</span>
                    </div>
                </Gallery>
                
                
            )
        }    
    }
}

export default Home;