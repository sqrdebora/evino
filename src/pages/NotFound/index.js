import React, { Component } from 'react'; 
import ufo from '../../utils/ufo.svg'
import styled from 'styled-components'
import { Link } from 'react-router-dom';
import {colors} from '../../utils/colors'

const Icon = styled.div `
    width: 100px;
    height: 100px;
    background: #000; 
    border-radius: 50%;
    overflow: hidden;
    margin: 0 auto;
    position: relative;
    img {
        width: 40px;
        animation: moveUfo 1.5s linear infinite;
        position: absolute; 
        left: 0;
        top: 30%; 
    }
`

const Text = styled.div `
    color: #fff; 
    text-align: center;
`

const Button = styled.button `
    margin: 20px auto;
    padding: 10px 0;
    letter-spacing: 1px;  
    display: block;
    width: 200px;
    background: ${colors.default}; 
    color: #fff;
    border: 0; 
    text-align: center; 
    a, a:hover {
        text-decoration: none;
        color: #fff;
    }
`
class NotFound extends Component {
    render() {
        return(
            <div>
                <Icon>
                    <img src={ufo} alt="ufo" />
                </Icon>
                <Text>
                    It looks like this page has been abducted.<br/>
                    The page does not exist, or cannot be found.
                </Text>
                <Button>
                    <Link to="/">go to home</Link>
                </Button>

            </div>
        )
    }
}

export default NotFound;