import React from 'react'; 
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import Header from './components/Header'
import Home from './pages/Home'
import Detail from './pages/Detail'
import NotFound from './pages/NotFound'
const Routes = () => {
    return(
        <BrowserRouter>
            <Header />
            <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/detail/:date" component={ Detail } />
                <Route path="*" component={ NotFound } />
            </Switch> 
        </BrowserRouter>
    );
}

export default Routes;