const size = {
    xs: '767px', 
    sm: '768px',
    md: '992px', 
    lg: '1200px'

  }

  export const device = {
    xs: `(max-width: ${size.xs})`,
    sm: `(min-width: ${size.sm})`,
    md: `(min-width: ${size.md})`,
    lg: `(min-width: ${size.lg})`,
  };