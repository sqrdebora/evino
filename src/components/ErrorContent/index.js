import React, { Component } from 'react'; 
import styled from 'styled-components'

const Content = styled.div`
  text-align: center;
`

const ErrorText = styled.div `
  font-size: 1rem;
  color: #fff;
  font-size: 1rem;
  line-height: 1.5rem;
`
const BlackHole = styled.div `
    width: 50px; 
    height: 50px; 
    
    border-radius: 50%; 
    background: #000;
    position: relative; 
    margin: 10px auto 20px;
    box-shadow: 5px 0 35px 10px #f50, 
       inset -5px 0 10px 10px #f80,
             -3px 0 10px 0 #f60;
             animation: rotating 4.5s linear infinite;
             filter: blur(3px);
        }
        &::after {
            content: '';
            width: 10px;
            height: 10px;
            left: 40vmin;
            box-shadow: #ffff0b85 0px 0 10px 10px,
                    #ffff0b85 1px 30px 10px 5px;
        }

        &::before {
            width: 50px; 
            height: 50px; 
            content: ''; 
            background: #000;
            display: block;
            border-radius: 50%; 
        }
        
`

class ErrorContent extends Component {
    render() {
        return(
            <Content>
                <ErrorText>
                    <BlackHole />
                    Oops :(<br/>
                    It looks likes a black hole.<br/>
                    Sorry, something wrong occurred.<br/>
                    Try again, please.<br/>
                </ErrorText>
            </Content>
        )
    }
}

export default ErrorContent;