import React, { Component } from 'react'; 
import styled from 'styled-components'
const Content = styled.div `
    position: relative; 
    height: 100px; 
    width: 100px; 
    border: 1px solid #fff;
    border-radius: 50%;
    margin: 10px auto
  }`
  
  const Star = styled.div `
    position: absolute; 
    top: 50%; 
    left: 50%; 
    transform: translate(-50%,-50%);
    background: #FACE2A; 
    width: 40px; 
    height: 40px;
    border-radius: 50%;  
  &::after {
    position: absolute; 
    width: 50px; 
    height: 50px; 
    background: #FACE2A; 
    content: ''; 
    border-radius: 50%; 
    top: 50%; 
    left: 50%; 
    transform: translate(-50%,-50%); 
    opacity: 0.15
  } 
  &::before {
    position: absolute; 
    width: 68px; 
    height: 68px; 
    background: #FACE2A; 
    content: ''; 
    border-radius: 50%; 
    top: 50%; 
    left: 50%; 
    transform: translate(-50%,-50%); 
    opacity: 0.15
  }`
  
  const Planet = styled.div `
    height: 100px; 
    width: 100px; 
    border-radius: 50%;
    position: absolute;
    top: 0;
    animation: rotating 7.5s linear infinite;
    &::after {
        width: 20px;
        height: 20px; 
        background: #37ABF9; 
        content: ''; 
        display: block; 
        border-radius: 50px;
        top: 50%;
        left: 50%;
        position: absolute; 
        margin: -58% -42%;
        border: 5px solid #4f4b58
    }`
  
class Loading extends Component {
    render() {
        return(
            <Content>
                <Star></Star>
                <Planet></Planet>
            </Content>
        )
    }
}

export default Loading;