import React, { Component } from 'react'; 
import { Link } from 'react-router-dom';
import styled from 'styled-components'
import logo from '../../utils/logo.svg'
const Logo = styled.div`
    text-align: center;
    margin: 20px 0 0 0;
    img {
        width: 150px;
    }    
`

const Title = styled.h1 `
    font-size: 1.4rem;
    letter-spacing: 2px;
    font-weight: normal; 
    text-align: center;
    color: #fff;
`

const SubTitle = styled.h2 `
    font-size: 1.1rem; 
    letter-spacing: 1px; 
    color: #fff; 
    font-weight: normal;
    text-align: center;
    margin: 0 0 40px 0;
`

class Header extends Component {
    render() {
        return(
            <div>
                <Link to="/">
                    <Logo>
                        <img src={logo} alt="logo" />
                    </Logo>
                </Link>
                
                <Title>Astronomy Picture of the Day</Title>
                <SubTitle>Discover the cosmos! Each day a different image or photograph of our fascinating universe is featured, along with a brief explanation written by a professional astronomer.</SubTitle>                
            </div>
        )
    }
}

export default Header;